<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Portfolio;
use App\Service;
use App\People;

use DB;

class IndexController extends Controller
{
    public function execute(Request $request){



        if($request->isMethod('post')){

            dd($request);

            $message = [
                'required' => 'Поле :attribute долждно быть заполненым',
                'email' => 'Поле :attribute должно соответствовать корректоному email адресу'
            ];

            $this->validate($request,[
                'name'  => 'required|max:255',
                'email' => 'required|email',
                'text'  => 'required'
            ], $message);

        }

        $pages = Page::all();
        $portfolios = Portfolio::all();
        $services = Service::where('id', '<', 20)->get();
        $people = People::take(3)->get();

        $tags = DB::table('portfolios')->distinct()->pluck('filter');


        $menu = [];

        foreach($pages as $page){
            $item = ['title' => $page->name , 'alias' => $page->alias];
            array_push($menu, $item);
        }

        $item = ['title' => 'Services', 'alias' => 'service'];
        array_push($menu, $item);

        $item = ['title' =>'Portfolio', 'alias' => 'Portfolio'];
        array_push($menu, $item);

        $item = ['title' => 'Team', 'alias'=> 'team'];
        array_push($menu, $item);

        $item = ['title' => 'Contact', 'alias' => 'contact'];
        array_push($menu, $item);


         return view('site.index', [
             'menu'       => $menu,
             'pages'      => $pages,
             'portfolios' => $portfolios,
             'services'   => $services,
             'people'     => $people,
             'tags'       => $tags,

         ]);
    }

    public function create(Request $request){

        return response()->json('true');

    }
}
